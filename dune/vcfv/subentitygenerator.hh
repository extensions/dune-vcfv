#ifndef DUNE_VCFV_SUBENTITYGENERATOR_HH
#define DUNE_VCFV_SUBENTITYGENERATOR_HH

#include<list>
#include<map>

#include<dune/geometry/multilineargeometry.hh>

#include<dune/vcfv/geometry.hh>
#include<dune/vcfv/referenceconfiguration.hh>
#include<dune/vcfv/subentity.hh>

namespace Dune {
  namespace VCFV {
    /** \brief class generating subvolumes/-interfaces for given base entities
     *
     *  Generates `SubvolumeList`s and `SubinterfaceList`s containing all subvolumes/-interfaces for a particular base cell.
     *  Those lists are iterable and can eg be used in ranged-based-for-loops to perform calculations on each subentity. Prefered usage:
     *  ```cpp
     *  auto subvolumes = generator_.getSubvolumeList();
     *  generator_.createSubvolumes(cell,subvolumes);
     *  auto subinterfaces = generator_.getSubinterfaceList();
     *  generator_.createSubinterfaces(cell,subvolumes,subinterfaces);
     *  ```
     *
     *  \tparam GV the GridView being used
     */
    template<typename GV>
    class Subentitygenerator {
    private:
      using SV = Subvolume<GV>;
      using SubvolumeList = std::vector<std::shared_ptr<SV>>;
      using SI = Subinterface<GV>;
      using SubinterfaceList = std::vector<std::shared_ptr<SI>>;
      using SF = Subface<GV>;
      using SubfaceList = std::vector<std::shared_ptr<SF>>;
      using Info = SubelementReferenceInfo<typename GV::ctype,GV::dimension>;
      using ReferenceElementCache = std::map<GeometryType,Info>;
      using FaceInfo = SubelementReferenceInfo<typename GV::ctype,GV::dimension-1>;
      using FaceReferenceElementCache = std::map<GeometryType,FaceInfo>;

      //helper for generating necessary reference elements on-the-fly
      template<typename Entity>
      const Info getReferenceElement(const Entity& cell) const {
        typename ReferenceElementCache::iterator it = referenceElementCache_.find(cell.type());
        if (it!=referenceElementCache_.end()) //reference element already cached
          return it->second;
        it = referenceElementCache_.insert(std::make_pair(cell.type(),Info(cell.type()))).first;
        return it->second;
      }

      //helper for generating necessary reference elements on-the-fly
      template<typename Entity>
      const FaceInfo getFaceReferenceElement(const Entity& face) const {
        typename FaceReferenceElementCache::iterator it = faceReferenceElementCache_.find(face.type());
        if (it!=faceReferenceElementCache_.end()) //reference element already cached
          return it->second;
        it = faceReferenceElementCache_.insert(std::make_pair(face.type(),FaceInfo(face.type()))).first;
        return it->second;
      }

    public:
      //! returns an empty `SubvolumeList`
      SubvolumeList getSubvolumeList() const
      { return SubvolumeList(); }

      //! returns an empty `SubinterfaceList`
      SubinterfaceList getSubinterfaceList() const
      { return SubinterfaceList(); }

      //! returns an empty `SubfaceList`
      SubfaceList getSubfaceList() const
      { return SubfaceList(); }

      /** \brief generates all subvolumes of a given base entity
       *
       *  \param cell the base cell
       *  \param subvolumes an (empty) `SubvolumeList` (as recieved from `getSubvolumeList()`)
       */
      template<typename Entity>
      void createSubvolumes(const Entity& cell, SubvolumeList& subvolumes) const {
        const auto& ref = getReferenceElement(cell);
        const auto codim = 0;
        using Traits = SubentityTraits<GV,codim>;
        using GeometryImp = typename Traits::Geometry;
        const auto dimension = Traits::globalDimension;
        //get cell information
        const auto& cellGeometry = cell.geometry();

        for(std::size_t sv=0; sv<ref.subvolumes(); ++sv) {
          //gather global vertex positions
          std::vector<typename Traits::GlobalCoordinate> corners;
          for(std::size_t v=0; v<ref.subvolumeVertices(); ++v)
            corners.push_back(cellGeometry.global(ref.subvolumeVertex(sv,v)));
          //generate geometry from vertices
          GeometryImp geometry(Dune::geometryTypeFromVertexCount(dimension-codim, corners.size()), corners);
          auto vol = std::make_shared<SV>(SubvolumeGeometry<GV>(std::move(geometry), cell, ref.subvolumeGeometry(sv)),
                                          ref.controllingIdx(sv),ref.subinterfaceIndices(sv));
          subvolumes.push_back(vol);
        }
      }

      /** \brief generates all subinterfaces of a given base entity
       *
       *  \param cell the base cell
       *  \param subvolumes a `SubvolumeList` containing all subvolumes for this cell
       *  \param subinterfaces an (empty) `SubinterfaceList` (as recieved from `getSubinterfaceList()`)
       */
      template<typename Entity>
      void createSubinterfaces(const Entity& cell, SubvolumeList& subvolumes, SubinterfaceList& subinterfaces) const {
        const auto codim = 1;
        using Traits = SubentityTraits<GV,codim>;
        using GeometryImp = typename Traits::Geometry;
        const auto dimension = Traits::globalDimension;
        //get cell information
        const auto& cellGeometry = cell.geometry();
        const auto& ref = getReferenceElement(cell);

        for(std::size_t si=0; si<ref.subinterfaces(); ++si) {
          //gather global vertex positions
          std::vector<typename Traits::GlobalCoordinate> corners;
          for(std::size_t v=0; v<ref.subinterfaceVertices(); ++v)
            corners.push_back(cellGeometry.global(ref.subinterfaceVertex(si,v)));
          //generate geometry from vertices
          GeometryImp geometry(Dune::geometryTypeFromVertexCount(dimension-codim, corners.size()), corners);
          //apply Piola transformation
          auto J = cellGeometry.jacobianInverseTransposed(ref.subinterfaceGeometry(si)->center());
          typename Info::NormalType normal(0.0);
          J.umv(ref.normal(si),normal);
          normal /= normal.two_norm();
          auto interface = std::make_shared<SI>(SubinterfaceGeometry<GV>(std::move(geometry),
                                                                         ref.subinterfaceGeometry(si),
                                                                         ref.insideGeometry(si),
                                                                         ref.outsideGeometry(si),
                                                                         std::move(normal)),
                                                subvolumes[ref.insideIdx(si)], subvolumes[ref.outsideIdx(si)]);
          subinterfaces.push_back(interface);
        }
      }

      /** \brief generates all subfaces of a given base entity
       *
       *  \param cell the base cell
       *  \param subfaces an (empty) `SubfaceList` (as recieved from `getSubfaceList()`)
       */
      template<typename Entity>
      void createSubfaces(const Entity& face, SubfaceList& subfaces) const {
        const auto& ref = getFaceReferenceElement(face);
        const auto codim = 1;
        using Traits = SubentityTraits<GV,codim>;
        using GeometryImp = typename Traits::Geometry;
        const auto dimension = Traits::globalDimension;
        //get cell information
        const auto& faceGeometry = face.geometry();
        const auto& facegeoInCell = face.geometryInInside();

        for(std::size_t sf=0; sf<ref.subvolumes(); ++sf) {
          //gather global vertex positions
          std::vector<typename Traits::GlobalCoordinate> corners;
          for(std::size_t v=0; v<ref.subvolumeVertices(); ++v)
            corners.push_back(faceGeometry.global(ref.subvolumeVertex(sf,v)));
          //generate geometry from vertices
          GeometryImp geometry(Dune::geometryTypeFromVertexCount(dimension-codim, corners.size()), corners);

          corners.clear();
          for(std::size_t v=0; v<ref.subvolumeVertices(); ++v)
            corners.push_back(facegeoInCell.global(ref.subvolumeVertex(sf,v)));
          //generate geometry from vertices
          std::shared_ptr<GeometryImp> sfgeoInCell = std::make_shared<GeometryImp>(Dune::geometryTypeFromVertexCount(dimension-codim, corners.size()), corners);

          auto refElement = Dune::referenceElement<typename Traits::ctype,dimension>(face.inside().type());
          std::size_t DOFindex = refElement.subEntity(face.indexInInside(),codim, sf, dimension);
          auto subface = std::make_shared<SF>(SubfaceGeometry<GV>(std::move(geometry), sfgeoInCell,
                                                                  face.centerUnitOuterNormal()),
                                              DOFindex);
          subfaces.push_back(subface);
        }
      }

    private:
      mutable ReferenceElementCache referenceElementCache_;
      mutable FaceReferenceElementCache faceReferenceElementCache_;
    };
  } //namespace VCFV
} //namespace Dune
#endif // DUNE_VCFV_SUBENTITYGENERATOR_HH
