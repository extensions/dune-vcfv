#ifndef DUNE_VCFV_GEOMETRY_HH
#define DUNE_VCFV_GEOMETRY_HH

#include<dune/geometry/multilineargeometry.hh>

namespace Dune {
  namespace VCFV {
    /** \brief basic traits class holding information on subentities
     *  \tparam GV the GridView being used
     *  \tparam codim the codimension of the subentity (wrt the base entity)
     */
    template<typename GV, std::size_t codim>
    struct SubentityTraits {
      using GridView = GV;
      //! Field type of domain coordinates
      using DomainField = typename GridView::ctype;
      using ctype = typename GridView::ctype;
      //! Local coordinate
      using LocalCoordinate = Dune::FieldVector<DomainField,GV::dimension-codim>;
      //! Global coordinate
      using GlobalCoordinate = Dune::FieldVector<DomainField,GV::dimension>;

      //! Dimension of the grid
      enum { globalDimension=GV::dimension };
      //! Dimension of the subentity
      enum { dimension=globalDimension-codim };

      //! Implementation of all relevant geometry mappings
      using Geometry = Dune::CachedMultiLinearGeometry<DomainField, dimension, globalDimension>;
      //! Type of the base entity
      using Entity = typename GV::template Codim<0>::Entity;
    };

    /** \brief generic geometry class for subentities
     *  \tparam GV the GridView being used
     *  \tparam codim the codimension of the subentity (wrt the base entity)
     */
    template<typename GV, std::size_t codim>
    class SubentityGeometry {
    public:
      using Traits = SubentityTraits<GV,codim>;
      using GeometryImp = typename Traits::Geometry;
      using Entity = typename Traits::Entity;
      using GlobalCoordinate = typename Traits::GlobalCoordinate;

      SubentityGeometry(const GeometryImp& geometry, std::shared_ptr<GeometryImp>& geometryInCell)
        : geometry_(geometry), geometryInCell_(geometryInCell)
      {}

      //! returns geometry mapping to global coordinates
      const GeometryImp& geometry() const
      { return geometry_; }

      //! returns the i-th corner in global coordinates
      GlobalCoordinate corner(std::size_t i) const
      { return geometry_.corner(i); }

      //! returns geometry mapping to base entity coordinates
      const GeometryImp& geometryInCell() const
      { return *geometryInCell_; }

      //! returns pointer to the geometry mapping to base entity coordinates
      const std::shared_ptr<GeometryImp>
      geometryInCellPointer() const
      { return geometryInCell_; }

    protected:
      const GeometryImp geometry_;
      const std::shared_ptr<GeometryImp> geometryInCell_;
    };

    /** \brief specialized geometry class for subvolumes
     *  \tparam GV the GridView being used
     */
    template<typename GV>
    class SubvolumeGeometry : public SubentityGeometry<GV,0> {
    public:
      using Base = SubentityGeometry<GV,0>;
      using Entity = typename Base::Entity;
      using GeometryImp = typename Base::GeometryImp;

      SubvolumeGeometry(const GeometryImp& geometry, const Entity& entity, std::shared_ptr<GeometryImp> geometryInCell)
        : Base(geometry, geometryInCell), entity_(entity)
      {}

      //! returns the base entity
      const Entity& entity() const
      { return entity_; }

    private:
      const Entity entity_;
    };

    /** \brief specialized geometry class for subinterfaces
     *  \tparam GV the GridView being used
     */
    template<typename GV>
    class SubinterfaceGeometry : public SubentityGeometry<GV,1> {
    public:
      using Base = SubentityGeometry<GV,1>;
      using Entity = typename Base::Entity;
      using GeometryImp = typename Base::GeometryImp;
      using NormalType = Dune::FieldVector<typename Base::Traits::ctype, Base::Traits::globalDimension>;

      template<typename Geo>
      SubinterfaceGeometry(const GeometryImp& geometry, std::shared_ptr<Geo> geometryInCell,
                           std::shared_ptr<Geo> geometryInInside, std::shared_ptr<Geo> geometryInOutside, const NormalType& normal)
        : Base(geometry, geometryInCell),
          geometryInInside_(geometryInInside), geometryInOutside_(geometryInOutside),
          unitOuterNormal_(normal)
      {}

      //! returns geometry mapping to inside subvolume coordinates
      const GeometryImp& geometryInInside() const
      { return *geometryInInside_; }

      //! returns pointer to geometry mapping to inside subvolume coordinates
      const std::shared_ptr<GeometryImp>
      geometryInInsidePointer() const
      { return geometryInInside_; }

      //! returns geometry mapping to outside subvolume coordinates
      const GeometryImp& geometryInOutside() const
      { return *geometryInOutside_; }

      //! returns pointer to geometry mapping to outside subvolume coordinates
      const std::shared_ptr<GeometryImp>
      geometryInOutsidePointer() const
      { return geometryInOutside_; }

      //! returns the unit outer normal in the center of the interface
      const NormalType unitOuterNormal() const
      { return unitOuterNormal_; }

    private:
      const std::shared_ptr<GeometryImp> geometryInInside_;
      const std::shared_ptr<GeometryImp> geometryInOutside_;
      const NormalType unitOuterNormal_;
    };

    /** \brief specialized geometry class for subfaces
     *  \tparam GV the GridView being used
     */
    // TODO: do we need a geometryInSubvolume?
    template<typename GV>
    class SubfaceGeometry : public SubentityGeometry<GV,1> {
    public:
      using Base = SubentityGeometry<GV,1>;
      using Entity = typename Base::Entity;
      using GeometryImp = typename Base::GeometryImp;
      using NormalType = Dune::FieldVector<typename Base::Traits::ctype, Base::Traits::globalDimension>;

      template<typename Geo>
      SubfaceGeometry(const GeometryImp& geometry, std::shared_ptr<Geo> geometryInCell,
                      const NormalType& normal)
        : Base(geometry, geometryInCell), unitOuterNormal_(normal)
      {}

      //! returns the unit outer normal in the center of the interface
      const NormalType unitOuterNormal() const
      { return unitOuterNormal_; }

    private:
      const NormalType unitOuterNormal_;
    };
  } //namespace VCFV
} //namespace Dune
#endif // DUNE_VCFV_GEOMETRY_HH
