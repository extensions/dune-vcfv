#ifndef DUNE_VCFV_PDELAB_MEAN_HH
#define DUNE_VCFV_PDELAB_MEAN_HH

#include<dune/pdelab/common/functionwrappers.hh>

#include<dune/vcfv/subentitygenerator.hh>
#include<dune/vcfv/utility.hh>

namespace Dune {
  namespace VCFV {
    // Calculate the mean of a vcfv-function
    // TODO: no need to pass a gridfunction -> evaluation of piecewise constant vcfv-functions only needs the DOFvector
    template<typename U, typename Generator = typename Dune::VCFV::Subentitygenerator<typename U::Traits::GridViewType>>
    auto mean(const U& u, const Generator& generator = Generator())
    {
      // loop over grid view
      typename U::Traits::RangeFieldType sum = 0.0;
      for(const auto& cell : elements(u.getGridView())) {
        auto subvolumes = generator.getSubvolumeList();
        generator.createSubvolumes(cell,subvolumes);
        for(const auto& sv : subvolumes) {
          auto& geo = sv->geometry();
          // evaluate the given grid function at subvolume-midpoint
          typename U::Traits::RangeType u_val;
          u.evaluate(cell,sv->geometryInCell().corner(0),u_val); //doesnt work for pyramids :/ -> TODO: add controllingCorner() to subentityGeometry
          sum += u_val*volume(geo);
        }
      }
      return sum;
    }
  } //namespace VCFV
} //namespace Dune
#endif // DUNE_VCFV_PDELAB_MEAN_HH
