#ifndef DUNE_VCFV_PDELAB_INTERPOLATION_HH
#define DUNE_VCFV_PDELAB_INTERPOLATION_HH

#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/common/quadraturerules.hh>
#include<dune/pdelab/function/localfunction.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include<dune/localfunctions/common/virtualinterface.hh>

#include<dune/vcfv/subentitygenerator.hh>
#include<dune/vcfv/utility.hh>

//TODO: documentation
namespace Dune {
  namespace VCFV {
    // this is the leaf version. xg has to be empty!
    template<typename F, typename GFS, typename XG, typename Generator = typename Dune::VCFV::Subentitygenerator<typename F::Traits::GridViewType>>
    void interpolate(const F& f, const GFS& gfs, XG& xg, std::size_t quadratureOrder = 1, const Generator& generator = Generator()) {
      //cell local defines
      using LFS = Dune::PDELab::LocalFunctionSpace<GFS>;
      using LFSCache = Dune::PDELab::LFSIndexCache<LFS>;
      using XView = typename XG::template LocalView<LFSCache>;
      XG weights(gfs,0.0);

      LFS lfs(gfs);
      LFSCache lfs_cache(lfs);
      XView x_view(xg);
      XView weights_view(weights);

      for(const auto& cell : elements(gfs.gridView())) {
        lfs.bind(cell);
        lfs_cache.update();
        x_view.bind(lfs_cache);
        weights_view.bind(lfs_cache);

        //generate subvolumes
        auto subvolumes = generator.getSubvolumeList();
        generator.createSubvolumes(cell,subvolumes);

        typename F::Traits::RangeType f_val;

        for(const auto& sv : subvolumes) {
          //get local index of this subvolume
          std::size_t localIdx = sv->controllingVertexIdx();
          //evaluate (global) function
          auto& geo = sv->geometry();
          auto& geoInCell = sv->geometryInCell();
          auto rule =  quadratureRule(geo, quadratureOrder);
          for(const auto& qp : rule) {
            f.evaluate(cell,geoInCell.global(qp.position()),f_val); //TODO: grid function for vcfv-functions
            //accumulate into local vector
            x_view[localIdx] +=  f_val * qp.weight() * geo.integrationElement(qp.position());
          }
          //accumulate volume of adjacent subvolumes into local vector
          weights_view[localIdx] += volume(geo);
        }
        x_view.unbind();
      }

      //global loop to apply weights
      auto weights_it = std::begin(weights);
      for(auto it = std::begin(xg); it != std::end(xg); ++it, ++weights_it) {
        *it /= *weights_it;
      }

      x_view.detach();
    }
  } //namespace VCFV
} //namespace Dune

#endif // DUNE_VCFV_PDELAB_INTERPOLATION_HH
