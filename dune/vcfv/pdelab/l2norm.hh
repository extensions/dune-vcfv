#ifndef DUNE_VCFV_PDELAB_L2NORM_HH
#define DUNE_VCFV_PDELAB_L2NORM_HH

#include<dune/pdelab/common/functionwrappers.hh>

#include<dune/vcfv/subentitygenerator.hh>
#include<dune/vcfv/utility.hh>

namespace Dune {
  namespace VCFV {
    // Calculate the squared L2 norm of a vcfv-function
    //TODO: no need to pass a gridfunction -> evaluation of piecewise constant vcfv-functions only needs the DOFvector
    template<typename U, typename Generator = typename Dune::VCFV::Subentitygenerator<typename U::Traits::GridViewType>>
    auto l2norm2(const U& u, const Generator& generator = Generator())
    {
      // loop over grid view
      typename U::Traits::RangeFieldType sum = 0.0;
      for(const auto& cell : elements(u.getGridView())) {
        auto subvolumes = generator.getSubvolumeList();
        generator.createSubvolumes(cell,subvolumes);
        for(const auto& sv : subvolumes) {
          auto& geo = sv->geometry();
          // evaluate the given grid function at subvolume-midpoint
          typename U::Traits::RangeType u_val;
          u.evaluate(cell,sv->geometryInCell().corner(0),u_val); //doesnt work for pyramids :/ -> TODO: add controllingCorner() to subentityGeometry
          // accumulate error
          sum += u_val.two_norm2()*volume(geo);
        }
      }
      return sum;
    }

    template<typename U, typename Generator = typename Dune::VCFV::Subentitygenerator<typename U::Traits::GridViewType>>
    auto l2norm(const U& u, const Generator& generator = Generator())
    {
      return std::sqrt(l2norm2(u,generator));
    }

    // Calculate the squared L2 differerence of two functions
    template<typename U, typename V, typename Generator = typename Dune::VCFV::Subentitygenerator<typename U::Traits::GridViewType>>
    auto l2difference2(const U& u, const V& v, const Generator& generator = Generator())
    {
      return l2norm2(Dune::PDELab::makePointwiseGridFunctionAdapter(
                       Dune::PDELab::PointwiseSumAdapterEngine(),u,
                       Dune::PDELab::makePointwiseGridFunctionAdapter(
                         Dune::PDELab::makePointwiseScaleAdapterEngine(
                           (typename V::Traits::RangeFieldType)(-1)),v)),
                     generator);
    }

    // Calculate the L2 differerence of two functions
    template<typename U, typename V, typename Generator = typename Dune::VCFV::Subentitygenerator<typename U::Traits::GridViewType>>
    auto l2difference(const U& u, const V& v, const Generator& generator = Generator())
    {
      return std::sqrt(l2difference2(u,v,generator));
    }

  } //namespace VCFV
} //namespace Dune
#endif // DUNE_VCFV_PDELAB_L2NORM_HH
