#ifndef DUNE_VCFV_REFERENCECONFIGURATION_HH
#define DUNE_VCFV_REFERENCECONFIGURATION_HH

#include<algorithm> //for std::set_intersection
#include<iostream>
#include<vector>

#include<dune/geometry/multilineargeometry.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/utility/typefromvertexcount.hh>

//currently supports simplices, cubes, pyramids and prisms. 2d and 3d will be tested
namespace Dune {
  namespace VCFV {
    namespace Impl {
      struct SubvolumeInfo {
      public:
        std::vector<std::size_t> vertexIndices;
        std::vector<std::size_t> intersectionIndices;
      };

      struct SubinterfaceInfo {
      public:
        std::vector<std::size_t> vertexIndices;
        std::size_t insideIndex;
        std::size_t outsideIndex;
      };
    } //namespace Impl

    template<typename ctype,std::size_t dim>
    class SubelementReferenceInfo {
    public:
      using GlobalCoordinate = Dune::FieldVector<ctype,dim>;
      using NormalType = Dune::FieldVector<ctype,dim>;
    private:
      using GlobalVertexList = std::vector<GlobalCoordinate>;
      using GlobalNormalList = std::vector<NormalType>;
      using SubvolumeIndexInfo = std::vector<Impl::SubvolumeInfo>;
      using SubinterfaceIndexInfo = std::vector<Impl::SubinterfaceInfo>;
      using SVGeometryImp = Dune::CachedMultiLinearGeometry<ctype, dim, dim>;
      using SIGeometryImp = Dune::CachedMultiLinearGeometry<ctype, dim-1, dim>;

    public:
      SubelementReferenceInfo(const GeometryType& geotype) : geotype_(geotype) {
        auto refElement = Dune::referenceElement<ctype,dim>(geotype);
        const bool isPyramid = geotype.isPyramid();
        if(!geotype.isCube() && !geotype.isSimplex() && !geotype.isPrism() && !isPyramid)
          DUNE_THROW(Dune::Exception, "Referenceelement: GeometryType " << geotype << " not (yet) supported");

        //controlvolume attached to the tip is divided into 4 subpyramids
        const std::size_t numSubvolumesPyr = 8;

        const std::size_t numVertices = refElement.size(dim);
        subvolumeInfo_.resize(isPyramid ? numSubvolumesPyr : numVertices);

        std::size_t currentGlobalIdx = 0;
        std::bitset<dim> insertionIdx;
        auto indexSet = getIndexSet(geotype,numVertices);

        //add DOFvertices separately
        for(std::size_t dof=0; dof<(isPyramid ? numVertices-1 : numVertices); ++dof) {
          auto center = refElement.position(dof,dim);
          globalVertices_.push_back(center);
          subvolumeInfo_[dof].vertexIndices.resize(std::pow(2,dim));
          subvolumeInfo_[dof].vertexIndices[0] = currentGlobalIdx;
          ++currentGlobalIdx;
        }
        if(isPyramid) {
          auto center = refElement.position(numVertices,dim);
          globalVertices_.push_back(center);
          for(std::size_t subpyr=numVertices-1; subpyr<numSubvolumesPyr; ++subpyr) {
            subvolumeInfo_[subpyr].vertexIndices.resize(5);
            subvolumeInfo_[subpyr].vertexIndices[4] = currentGlobalIdx;
          }
          ++currentGlobalIdx;
        }

        for(std::size_t codim=0; codim<dim; ++codim) {
          for(std::size_t i=0; i<refElement.size(codim); ++i) {
            auto center = refElement.position(i,codim);
            globalVertices_.push_back(center);
            //iterate vertices of current entity
            auto adjVertices = refElement.subEntities(i,codim,dim);
            for(auto dof : adjVertices) {
              if(!(isPyramid && dof==4)) {
                insertionIdx.reset();
                //check whether vertex v is in the indexSet of the DOFvertices
                for(std::size_t idx=0; idx<dim; ++idx) {
                  if(adjVertices.contains(indexSet[dof][idx]))
                    insertionIdx[idx] = true;
                }
                assert(insertionIdx.count()==(dim-codim));
                subvolumeInfo_[dof].vertexIndices[insertionIdx.to_ulong()] = currentGlobalIdx;
              }
              else {
                for(std::size_t subpyr=numVertices-1; subpyr<numSubvolumesPyr; ++subpyr) {
                  insertionIdx.set();
                  for(std::size_t idx=0; idx<dim; ++idx) {
                    if(adjVertices.contains(indexSet[subpyr][idx]))
                      insertionIdx[idx] = false;
                  }
                  assert(insertionIdx.count()==(dim-codim));
                  subvolumeInfo_[subpyr].vertexIndices[insertionIdx.to_ulong()] = currentGlobalIdx;
                }
              }
            }
            ++currentGlobalIdx;
          }
        }

        //generate intersections
        std::size_t maxIntersectionSize = 0;
        std::vector<std::size_t> indices1;
        std::vector<std::size_t> indices2;
        for(std::size_t i=0; i<subvolumeInfo_.size(); ++i) {
          for(std::size_t j=i+1; j<subvolumeInfo_.size(); ++j) {
            std::vector<std::size_t> intersection;
            indices1 = subvolumeInfo_[i].vertexIndices;
            std::sort(indices1.begin(),indices1.end());
            indices2 = subvolumeInfo_[j].vertexIndices;
            std::sort(indices2.begin(),indices2.end());
            std::set_intersection(indices1.begin(),indices1.end(),indices2.begin(),indices2.end(),
                                  std::back_inserter(intersection));
            if(intersection.size() >= maxIntersectionSize) {
              subinterfaceInfo_.push_back({intersection,i,j});
              maxIntersectionSize = intersection.size();
            }
          }
        }
        assert(maxIntersectionSize==std::pow(2,dim-1));
        for(auto it = subinterfaceInfo_.begin(); it!=subinterfaceInfo_.end();) {
          if(it->vertexIndices.size() < maxIntersectionSize)
            subinterfaceInfo_.erase(it);
          else
            ++it;
        }

        //set subvolume geometries
        for(std::size_t i=0; i<subvolumeInfo_.size(); ++i)
          subvolumeGeometries_.push_back(computeGeometryInCell(i));
        //set adjacent intersections for subvolumes and geometries
        for(std::size_t i=0; i<subinterfaceInfo_.size(); ++i) {
          auto insideIdx = subinterfaceInfo_[i].insideIndex;
          auto outsideIdx = subinterfaceInfo_[i].outsideIndex;
          subvolumeInfo_[insideIdx].intersectionIndices.push_back(i);
          subvolumeInfo_[outsideIdx].intersectionIndices.push_back(i);
          subinterfaceGeometries_.push_back(computeGeometryInterfaceInCell(i));
          insideGeometries_.push_back(computeGeometryInSubvolume(i,subvolumeGeometries_[insideIdx]));
          outsideGeometries_.push_back(computeGeometryInSubvolume(i,subvolumeGeometries_[outsideIdx]));
        }

        //set normals
        /* this is hardcoded for dim=2,3. For higher dimensions/a more general code one could pick (dim-1) linear
           independent vectors in the interface and solve the associated linear system for the normal vector */
        for(auto& info : subinterfaceInfo_) {
          if(dim==1) {
            auto direction = subvolumeGeometries_[info.outsideIndex]->center() - subvolumeGeometries_[info.insideIndex]->center();
            direction /= direction.two_norm();
            globalNormals_.push_back(NormalType({direction}));
          }
          else if(dim==2) {
            auto dummy = globalVertices_[info.vertexIndices[0]]-globalVertices_[info.vertexIndices[1]];
            auto length = dummy.two_norm();
            NormalType normal({-dummy[1]/length, dummy[0]/length}); //rotate counterclockwise
            //make sure the normal points from inside() to outside(), flip otherwise
            auto direction = subvolumeGeometries_[info.outsideIndex]->center() - subvolumeGeometries_[info.insideIndex]->center();
            direction /= direction.two_norm();
            if(normal.dot(direction) >= 1e-10)
              globalNormals_.push_back(normal);
            else
              globalNormals_.push_back(NormalType({-normal[0],-normal[1]}));
          }
          else if(dim==3) {
            auto p0 = globalVertices_[info.vertexIndices[0]];
            auto p1 = globalVertices_[info.vertexIndices[1]];
            auto p2 = globalVertices_[info.vertexIndices[2]];
            auto v1 = p1-p0;
            auto v2 = p2-p1;

            NormalType dummy({v1[1]*v2[2] - v1[2]*v2[1], v1[2]*v2[0]-v1[0]*v2[2], v1[0]*v2[1]-v1[1]*v2[0]}); //cross product
            auto length = dummy.two_norm();
            NormalType normal({dummy[0]/length, dummy[1]/length, dummy[2]/length});
            //make sure the normal points from inside() to outside(), flip otherwise
            auto direction = subvolumeGeometries_[info.outsideIndex]->center() - subvolumeGeometries_[info.insideIndex]->center();
            direction /= direction.two_norm();
            if(normal.dot(direction) >= 1e-10)
              globalNormals_.push_back(normal);
            else
              globalNormals_.push_back(NormalType({-normal[0],-normal[1], -normal[2]}));
          }
          else {
            DUNE_THROW(Dune::Exception, "Normal calculation for dimension > 3 not implemented (who actually needs this?)");
          }
        }
      }

    private:
      //TODO: verify prism-implementation
      //TODO: verify pyramid-implementation
      /*generates helper structure which defines a righthanded-coordinatesystem for
        each subvolume (used to add subvolume vertices in DUNE-ordering)*/
      auto getIndexSet(const GeometryType& geotype, std::size_t numVertices) const {
        std::vector<std::vector<std::size_t>> indices;
        indices.resize(numVertices);

        if(dim==1) {
          indices[0] = {1};
          indices[1] = {0};
        }

        //proofed to work in any dimension
        else if(geotype.isSimplex()) {
          //hardcode 2d
          indices[0] = {1,2};
          indices[1] = {2,0};
          indices[2] = {0,1};

          for(std::size_t d=3; d<=dim; ++d) {
            //old vertices: augment existing indexsets by 'new' vertex
            for(std::size_t i=0; i<d; ++i)
              indices[i].push_back(d);
            //new vertex: use indices {1,...,dim-1}, add last
            //index (0) in first or second position depending on dimension (so the system stays righthanded)
            for(std::size_t i=1; i<dim; ++i)
              indices[d].push_back(i);
            auto it = indices[d].begin();
            if(((d+1)%2)==1)
              indices[d].insert(it,0);
            else
              indices[d].insert(it+1,0);
          }
        }

        else if(geotype.isCube()) {
          //hardcode 2d
          indices[0] = {1,2};
          indices[1] = {3,0};
          indices[2] = {0,3};
          indices[3] = {2,1};

          for(std::size_t d=3; d<=dim; ++d) {
              std::size_t indexShift = 1u<<(dim-1);
              for(std::size_t i=0; i<indexShift; ++i) {
              //new vertices: use shifted index set from vertex from which it was extruded. Add last
              //index in first or second position depending on dimension (so the system stays righthanded)
              indices[i+indexShift] = indices[i];
              for(std::size_t k=0; k<indices[i+indexShift].size(); ++k)
                indices[i+indexShift][k] += indexShift;
              auto it = indices[i+indexShift].begin();
              if(((d+1)%2)==1)
                indices[i+indexShift].insert(it,i);
              else
                indices[i+indexShift].insert(it+1,i);
              //old vertices: augment existing indexsets
              indices[i].push_back(i+indexShift);
            }
          }
        }

        else if(geotype.isPrism()) {
          assert(dim==3);
          indices[0] = {1,2,3};
          indices[1] = {2,0,4};
          indices[2] = {0,1,5};
          indices[3] = {4,0,5};
          indices[4] = {5,1,3};
          indices[5] = {3,2,4};
        }

        else if(geotype.isPyramid()) {
          indices[0] = {1,2,4};
          indices[1] = {3,0,4};
          indices[2] = {2,1,4};
          indices[3] = {0,3,4};
          /* index sets for the subpyramids - this is purely to keep DUNE-ordering of the subpyramid vertices and
             has nothing to do with righthanded coordinate systems anymore */
          indices[4] = {1,2,0};
          indices[5] = {3,0,1};
          indices[6] = {0,3,2};
          indices[7] = {2,1,3};
        }
        return indices;
      }

      //helper to create mapping subvolume->base cell
      std::shared_ptr<SVGeometryImp>
      computeGeometryInCell(std::size_t subvolumeIdx) {
        std::vector<GlobalCoordinate> cornersInCell(subvolumeVertices());
        for(std::size_t i=0; i<subvolumeVertices(); ++i)
          cornersInCell[i] = subvolumeVertex(subvolumeIdx,i);
        return std::make_shared<SVGeometryImp>(
                    Dune::geometryTypeFromVertexCount(dim, subvolumeVertices()),cornersInCell);
      }

      //helper to create the mapping subinterface->base cell
      std::shared_ptr<SIGeometryImp>
      computeGeometryInterfaceInCell(std::size_t subinterfaceIdx) {
        std::vector<GlobalCoordinate> cornersInCell(subinterfaceVertices());
        for(std::size_t i=0; i<subinterfaceVertices(); ++i)
          cornersInCell[i] = subinterfaceVertex(subinterfaceIdx,i);
        return std::make_shared<SIGeometryImp>(
                    Dune::geometryTypeFromVertexCount(dim-1, subinterfaceVertices()),cornersInCell);
      }

      //helper to create the mapping subinterface->subvolume
      template<typename SVGeo>
      std::shared_ptr<SIGeometryImp>
      computeGeometryInSubvolume(std::size_t subinterfaceIdx, const SVGeo& subvolumeGeo) {
        std::vector<GlobalCoordinate> cornersInSV(subinterfaceVertices());
        for(std::size_t i=0; i<subinterfaceVertices(); ++i)
          cornersInSV[i] = subvolumeGeo->local(subinterfaceVertex(subinterfaceIdx,i));
        return std::make_shared<SIGeometryImp>(
                    Dune::geometryTypeFromVertexCount(dim-1, subinterfaceVertices()),cornersInSV);
      }

    public:
      //! get total number of subvolumes
      std::size_t subvolumes() const
      { return subvolumeInfo_.size(); }

      //! get total number of subinterfaces
      std::size_t subinterfaces() const
      { return subinterfaceInfo_.size(); }

      //! get total number of vertices per subvolume
      std::size_t subvolumeVertices() const
      { return subvolumeInfo_[0].vertexIndices.size(); }

      //! get total number of vertices per subinterface
      std::size_t subinterfaceVertices() const
      { return subinterfaceInfo_[0].vertexIndices.size(); }

      //! get global coordinates of vertex with (subvolume specific) index 'vertexIdx' from the subvolume with index 'subvolumeIdx'
      GlobalCoordinate subvolumeVertex(std::size_t subvolumeIdx, std::size_t vertexIdx) const
      { return globalVertices_[subvolumeInfo_[subvolumeIdx].vertexIndices[vertexIdx]]; }

      //! get global coordinates of vertex with (subinterface specific) index 'vertexIdx' from the subinterface with index 'subinterfaceIdx'
      GlobalCoordinate subinterfaceVertex(std::size_t subinterfaceIdx, std::size_t vertexIdx) const
      { return globalVertices_[subinterfaceInfo_[subinterfaceIdx].vertexIndices[vertexIdx]]; }

      //! get all intersection indices of subvolume with index 'subvolumeIdx'
      std::vector<std::size_t> subinterfaceIndices(std::size_t subvolumeIdx) const
      { return subvolumeInfo_[subvolumeIdx].intersectionIndices; }

      //! get index of inside subvolume from subinterface with index 'subinterfaceIdx'
      std::size_t insideIdx(std::size_t subinterfaceIdx) const
      { return subinterfaceInfo_[subinterfaceIdx].insideIndex; }

      //! get index of outside subvolume from subinterface with index 'subinterfaceIdx'
      std::size_t outsideIdx(std::size_t subinterfaceIdx) const
      { return subinterfaceInfo_[subinterfaceIdx].outsideIndex; }

      //! get geometry mapping from subvolume to base cell
      const std::shared_ptr<SVGeometryImp> subvolumeGeometry(std::size_t subvolumeIdx) const
      { return subvolumeGeometries_[subvolumeIdx]; }

      //! get geometry mapping from subinterface to base cell
      const std::shared_ptr<SIGeometryImp> subinterfaceGeometry(std::size_t subinterfaceIdx) const
      { return subinterfaceGeometries_[subinterfaceIdx]; }

      //! get geometry mapping from subinterface to inside subvolume
      const std::shared_ptr<SIGeometryImp> insideGeometry(std::size_t subinterfaceIdx) const
      { return insideGeometries_[subinterfaceIdx]; }

      //! get geometry mapping from subinterface to outside subvolume
      const std::shared_ptr<SIGeometryImp> outsideGeometry(std::size_t subinterfaceIdx) const
      { return outsideGeometries_[subinterfaceIdx]; }

      //! get index of the vertex controliing the subvolume with index 'subvolumeIdx'
      std::size_t controllingIdx(std::size_t subvolumeIdx) const
      { return (geotype_.isPyramid() && subvolumeIdx>=4) ? 4 : subvolumeIdx; }

      //! get the unit outer normal for a specific subinterface
      const NormalType normal(std::size_t subinterfaceIdx) const
      { return globalNormals_[subinterfaceIdx]; }

      //! prints information (verbosity=1: print coordinates, verbosity=2: print indices)
      void printInfo(int verbosity=0) const {
        std::cout << "--- Reference information ---" << std::endl;
        std::cout << "Geometry type: " << geotype_ << std::endl;
        std::cout << "Global number of vertices: " << globalVertices_.size() << std::endl;
        if(verbosity >= 1) {
          std::cout << "Coordinates:" << std::endl;
          std::size_t i=0;
          for(auto v : globalVertices_) {
            std::cout << "-> Vertex " << i << ": " << v << std::endl;
            ++i;
          }
        }
        std::cout << "Number of subvolumes: " << subvolumes() << " with " << subvolumeVertices() << " vertices each" << std::endl;
        if(verbosity >= 1) {
          std::cout << "Coordinates: " << std::endl;
          std::size_t i=1;
          for(auto sv : subvolumeInfo_) {
            std::cout << "  Subvolume " << i << ": " << std::endl;
            for(std::size_t j=1; j<=subvolumeVertices(); ++j)
              std::cout << "  -> Vertex " << j << ": " <<  subvolumeVertex(i-1,j-1) << std::endl;
            if(verbosity >= 2) {
              std::cout << "  Vertex indices:";
              for(auto k : sv.vertexIndices)
                std::cout << " " << k;
              std::cout << std::endl;
            }
            ++i;
          }
        }
        std::cout << "Number of subinterfaces: " << subinterfaces() << " with " << subinterfaceVertices() << " vertices each" << std::endl;
        if(verbosity >= 1) {
          std::cout << "Coordinates: " << std::endl;
          std::size_t i=1;
          for(auto si : subinterfaceInfo_) {
            std::cout << "  Subinterface " << i << ": " << std::endl;
            for(std::size_t j=1; j<=subinterfaceVertices(); ++j)
              std::cout << "  -> Vertex " << j << ": " <<  subinterfaceVertex(i-1,j-1) << std::endl;
            if(verbosity >= 2) {
              std::cout << "  Vertex indices:";
              for(auto k : si.vertexIndices)
                std::cout << " " << k;
              std::cout << std::endl;
            }
            auto normal = globalNormals_[i-1];
            std::cout << "  -> Normal: ";
            for(auto x : normal)
              std::cout << x << " ";
            std::cout << std::endl;
            ++i;
          }
        }
        std::cout << "--- Reference information end ---" << std::endl;
      }

    protected:
      GeometryType geotype_;
      GlobalVertexList globalVertices_;
      GlobalNormalList globalNormals_;
      SubvolumeIndexInfo subvolumeInfo_;
      SubinterfaceIndexInfo subinterfaceInfo_;
      std::vector<std::shared_ptr<SVGeometryImp>> subvolumeGeometries_;
      std::vector<std::shared_ptr<SIGeometryImp>> subinterfaceGeometries_;
      std::vector<std::shared_ptr<SIGeometryImp>> insideGeometries_;
      std::vector<std::shared_ptr<SIGeometryImp>> outsideGeometries_;
    };
  } //namespace VCFV
} //namespace Dune
#endif // DUNE_VCFV_REFERENCECONFIGURATION_HH
