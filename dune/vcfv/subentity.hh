#ifndef DUNE_VCFV_SUBENTITY_HH
#define DUNE_VCFV_SUBENTITY_HH

#include<dune/geometry/multilineargeometry.hh>

#include<dune/vcfv/geometry.hh>

namespace Dune {
  namespace VCFV {
    /** \brief class containing all relevant information on a particular subvolume
     *  \tparam GV the GridView being used
     */
    template<typename GV>
    class Subvolume : public SubvolumeGeometry<GV> {
      using Geo = SubvolumeGeometry<GV>;
      using Indices = std::vector<std::size_t>;
    public:
      Subvolume(const Geo& geometry, const std::size_t controllingVertexIdx, const Indices& indices)
        : Geo(geometry), controllingVertexIdx_(controllingVertexIdx), subinterfaceIndices_(indices) {}

      //! returns the (global) index of the DOF this subvolume is assigned to
      std::size_t controllingVertexIdx()
      { return controllingVertexIdx_; }

      //! \deprecated this is rather ugly and I doubt it will ever be needed
      std::size_t subinterfaceIdx(std::size_t i)
      { return subinterfaceIndices_[i]; }

    private:
      std::size_t controllingVertexIdx_;
      Indices subinterfaceIndices_;
    };

    /** \brief class containing all relevant information on a particular subinterface
     *
     *  \tparam GV the GridView being used
     */
    template<typename GV>
    class Subinterface : public SubinterfaceGeometry<GV> {
      using Geo = SubinterfaceGeometry<GV>;
      using Traits = typename Geo::Traits;
      using SubvolumePtr = std::shared_ptr<Subvolume<GV>>;
    public:
      Subinterface(const Geo& geometry, const SubvolumePtr insideSubvolume, const SubvolumePtr outsideSubvolume)
        : Geo(geometry), insideSubvolume_(insideSubvolume), outsideSubvolume_(outsideSubvolume)
      {}

      //! returns a pointer to the inside subvolume
      SubvolumePtr inside() const
      { return insideSubvolume_; }

      //! returns a pointer to the outside subvolume
      SubvolumePtr outside() const
      { return outsideSubvolume_; }

    private:
      const SubvolumePtr insideSubvolume_;
      const SubvolumePtr outsideSubvolume_;
    };

      /** \brief class containing all relevant information on a particular subface
     *
     *  \tparam GV the GridView being used
     */
    template<typename GV>
    class Subface : public SubfaceGeometry<GV> {
      using Geo = SubfaceGeometry<GV>;
      using Traits = typename Geo::Traits;
      using SubvolumePtr = std::shared_ptr<Subvolume<GV>>;
    public:
      Subface(const Geo& geometry, const std::size_t controllingVertexIdx)
        : Geo(geometry), controllingVertexIdx_(controllingVertexIdx)
      {}

      //! returns the (global) index of the DOF this subvolume is assigned to
      std::size_t controllingVertexIdx()
      { return controllingVertexIdx_; }

   private:
      std::size_t controllingVertexIdx_;
    };
  } //namespace VCFV
} //namespace Dune
#endif // DUNE_VCFV_SUBENTITY_HH
