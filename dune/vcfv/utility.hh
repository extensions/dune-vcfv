#ifndef DUNE_VCFV_UTILITY_HH
#define DUNE_VCFV_UTILITY_HH

#include<dune/geometry/quadraturerules.hh>

namespace Dune {
  namespace VCFV {
    //Evaluate volume of cube-like subvolume
    template<typename SVGeo>
    auto volume(const SVGeo& geo) {
      const int dim = geo.mydimension;
      if(dim==2)
        return geo.volume();
      else {
        int intorder = 2;
        auto& rule = QuadratureRules<typename SVGeo::ctype,dim>::rule(geo.type(),intorder);
        double volume = 0.0;
        for(const auto& qp : rule)
          volume += geo.integrationElement(qp.position())*qp.weight();
        return volume;
      }
    }
  } //namespace VCFV
} //namespace Dune
#endif // DUNE_VCFV_UTILITY_HH
