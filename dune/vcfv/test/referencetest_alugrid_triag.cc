// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#if HAVE_DUNE_ALUGRID
#include<dune/alugrid/grid.hh>
#include<dune/grid/io/file/gmshreader.hh>
#endif

#include<dune/common/parallel/mpihelper.hh>

#include "referencetest.hh"

int main(int argc, char** argv) {
  Dune::MPIHelper::instance(argc, argv);

  const std::size_t verbosity = 0;

  const int dim = CMAKE_DIMENSION;

  auto filename = "meshes/" + std::to_string(dim) + "d/triag.msh";

  using Grid =  Dune::ALUGrid<dim, dim, Dune::simplex, Dune::nonconforming>;

  //generate grid
  Dune::GridFactory<Grid> factory;
  Dune::GmshReader<Grid>::read(factory, filename, true, true);
  std::shared_ptr<Grid> gridptr(factory.createGrid());

  //run test
  runTest(*gridptr,verbosity);
}
