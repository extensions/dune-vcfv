#ifndef DUNE_VCFV_TEST_SINOSOIDAL_DIFFUSION_HH
#define DUNE_VCFV_TEST_SINOSOIDAL_DIFFUSION_HH

#include<math.h>

namespace Test {
  template<int DIM>
  class HomogeneousDirichletBase {
  public:
    HomogeneousDirichletBase(Dune::ParameterTree& pTree) {
      //parameters of domain
      double Lx = pTree.get<double>("test.xdim");
      double Ly = pTree.get<double>("test.ydim");
      dim = {Lx, Ly};
      if(DIM==3)
        dim[3] = pTree.get<double>("test.zdim");

      //parameters of base solution
      double u_periods = pTree.get<double>("sinosoidal_ic.u_periods");
      u_factor = pTree.get<double>("sinosoidal_ic.scaling_factor");

      u_c_x = (u_periods * M_PI * 2.0 / dim[0]);
      u_c_y = (u_periods * M_PI * 2.0 / dim[1]);
    }

  private:
    double u_xpart(const Dune::FieldVector<double, DIM>& loc) const {
      return sin(u_c_x * loc[0]);
    }

    double u_ypart(const Dune::FieldVector<double, DIM>& loc) const {
      return sin(u_c_y * loc[1]);
    }

  public:
    inline double u(const Dune::FieldVector<double, DIM>& loc) const {
      return u_factor * u_xpart(loc) * u_ypart(loc);
    }

    inline double u_x(const Dune::FieldVector<double, DIM>& loc) const {
      return u_factor * u_c_x * cos(u_c_x * loc[0]) * u_ypart(loc);
    }

    inline double u_y(const Dune::FieldVector<double, DIM>& loc) const {
      return u_factor * u_c_y * cos(u_c_y * loc[1]) * u_xpart(loc);
    }

    //first derivatives du/dx_i for i>2 are all zero

    inline double u_xx(const Dune::FieldVector<double, DIM>& loc) const {
      return -u_factor * std::pow(u_c_x, 2) * sin(u_c_x * loc[0]) * u_ypart(loc);
    }

    inline double u_yy(const Dune::FieldVector<double, DIM>& loc) const {
      return -u_factor * std::pow(u_c_y, 2) * sin(u_c_y * loc[1]) * u_xpart(loc);
    }

    inline double u_xy(const Dune::FieldVector<double, DIM>& loc) const {
      return u_factor * u_c_x * cos(u_c_x * loc[0]) * u_c_y * cos(u_c_y * loc[1]);
    }

    std::array<double,DIM> dim; //dimensions of domain
  private:
    double u_c_x, u_c_y; //factor depending the number of periods
    double u_factor; //base amplitude
  };

  template<int DIM>
  class HomogeneousNeumannBase {
  public:
    HomogeneousNeumannBase(Dune::ParameterTree& pTree) {
      //parameters of domain
      double Lx = pTree.get<double>("test.xdim");
      double Ly = pTree.get<double>("test.ydim");
      dim = {Lx, Ly};
      if(DIM==3)
        dim[3] = pTree.get<double>("test.zdim");

      //parameters of base solution
      double u_periods = pTree.get<double>("sinosoidal_ic.u_periods");
      u_factor = pTree.get<double>("sinosoidal_ic.scaling_factor");

      u_c_x = (u_periods * M_PI * 2.0 / dim[0]);
      u_c_y = (u_periods * M_PI * 2.0 / dim[1]);
    }

  private:
    double u_xpart(const Dune::FieldVector<double, DIM>& loc) const {
      return cos(u_c_x * loc[0]);
    }

    double u_ypart(const Dune::FieldVector<double, DIM>& loc) const {
      return cos(u_c_y * loc[1]);
    }

  public:
    inline double u(const Dune::FieldVector<double, DIM>& loc) const {
      return u_factor * u_xpart(loc) * u_ypart(loc);
    }

    inline double u_x(const Dune::FieldVector<double, DIM>& loc) const {
      return u_factor * u_c_x * -sin(u_c_x * loc[0]) * u_ypart(loc);
    }

    inline double u_y(const Dune::FieldVector<double, DIM>& loc) const {
      return u_factor * u_c_y * -sin(u_c_y * loc[1]) * u_xpart(loc);
    }

    //first derivatives du/dx_i for i>2 are all zero

    inline double u_xx(const Dune::FieldVector<double, DIM>& loc) const {
      return u_factor * std::pow(u_c_x, 2) * -cos(u_c_x * loc[0]) * u_ypart(loc);
    }

    inline double u_yy(const Dune::FieldVector<double, DIM>& loc) const {
      return u_factor * std::pow(u_c_y, 2) * -cos(u_c_y * loc[1]) * u_xpart(loc);
    }

    inline double u_xy(const Dune::FieldVector<double, DIM>& loc) const {
      return u_factor * u_c_x * sin(u_c_x * loc[0]) * u_c_y * sin(u_c_y * loc[1]);
    }

    std::array<double,DIM> dim; //dimensions of domain
  private:
    double u_c_x, u_c_y; //factor depending the number of periods
    double u_factor; //base amplitude
  };
}

//TODO: documentation
template<class BaseSolution, int DIM>
class KernelProvider {
public:
  KernelProvider(Dune::ParameterTree& pTree, BaseSolution& baseSolution) : pTree(pTree), base(baseSolution) {
    //parameters of diffusionmatrix
    alpha_periods = pTree.get<double>("sinosoidal_ic.periods");
    amp_periods = pTree.get<double>("sinosoidal_ic.amplitude_periods");

    Lx = base.dim[0];
    Ly = base.dim[1];
    if(DIM==3)
      Lz = base.dim[2];
    alpha_c_x = (alpha_periods * M_PI * 2.0 / Lx);
    alpha_c_y = (alpha_periods * M_PI * 2.0 / Ly);
  }

  inline double alpha(const Dune::FieldVector<double, DIM>& loc) const {
    static const double amplitude = M_PI / 2.0;
    double result = amplitude * sin(alpha_periods * (M_PI * 2.0 / Lx) * std::pow(loc[0], 4))
                              * sin(alpha_periods * (M_PI * 2.0 / Ly) * std::pow(loc[1], 4));
    return result;
  }

  inline double dx_alpha(const Dune::FieldVector<double, DIM>& loc) const {
    static const double amplitude = M_PI / 2.0;
    double result = amplitude * alpha_c_x * 4.0 * std::pow(loc[0], 3) * cos(alpha_c_x * std::pow(loc[0], 4))
                                                                      * sin(alpha_c_y * std::pow(loc[1], 4));
    return result;
  }

  inline double dy_alpha(const Dune::FieldVector<double, DIM>& loc) const {
    static const double amplitude = M_PI / 2.0;
    double result = amplitude * alpha_c_y * 4.0 * std::pow(loc[1], 3) * cos(alpha_c_y * std::pow(loc[1], 4))
                                                                      * sin(alpha_c_x * std::pow(loc[0], 4));
    return result;
  }

  inline double gf(const double& x, const double& y) const {
    static const double a = pTree.get<double>("test.D_xx");
    return a * (1.1 + sin(amp_periods * (2.0 * M_PI / Lx) * x));
  }

  inline double dx_gf(const double& x, const double& y) const {
    static const double a = pTree.get<double>("test.D_xx");
    return a * amp_periods * (2.0 * M_PI / Lx) * cos(amp_periods * (2.0 * M_PI / Lx) * x);
  }

  inline double dy_gf(const double& x, const double& y) const {
    return 0.0;
  }

  inline double hf(const double& x, const double& y) const {
    static const double b = pTree.get<double>("test.D_yy");
    return b * (1.1 + sin(amp_periods * (2.0 * M_PI / Ly) * y));
  }

  inline double dy_hf(const double& x, const double& y) const {
    static const double b = pTree.get<double>("test.D_yy");
    return b * amp_periods * (2.0 * M_PI / Ly) * cos(amp_periods * (2.0 * M_PI / Ly) * y);
  }

  inline double dx_hf(const double& x, const double& y) const {
    return 0.0;
  }

  ///gives a field of inhomogeneous anisotropic Diffusion tensors. Used as a testcase. config-configurable
  template<typename POS>
  inline Dune::FieldMatrix<double, DIM, DIM> sinosoidal_D(const POS& global) const {
    Dune::FieldMatrix<double, DIM, DIM> D(0.0);

    double x = global[0];
    double y = global[1];

    //turn the anisotropy while moving in x-dir
    //turning will preserve the SPD properties of the matrix!
    //expression is turnmatrix* D_diag(gf(),h(f))*turnmatrix^t.
    const auto alph = alpha(global);
    const auto gfc = gf(x, y);
    const auto hfc = hf(x, y);

    D[0][0] = gfc * cos(alph) * cos(alph) + hfc * sin(alph) * sin(alph);
    D[0][1] = cos(alph) * gfc * sin(alph) - cos(alph) * hfc * sin(alph);
    D[1][0] = cos(alph) * gfc * sin(alph) - cos(alph) * hfc * sin(alph);
    D[1][1] = hfc * cos(alph) * cos(alph) + gfc * sin(alph) * sin(alph);

    //ToDo: adding D_20 and D_21 provides more testing capabilities without adding u_z and u_zz
    if(DIM==3)
      D[2][2] = 1.0;

    return D;
  }

  template<typename POS>
  inline Dune::FieldMatrix<double, DIM, DIM> dx_sinosoidal_D(const POS& global) const {
    //just hack the following into a CAD program of your choice (e.g. wolfram) for validation:
    // d/dx {{Cos[f(x,y)], -Sin[f(x,y)]}, { Sin[f(x,y)],Cos[f(x,y)]}}.{{g(x,y), 0}, {0, h(x,y)}}.{{Cos[f(x,y)], Sin[f(x,y)]}, { -Sin[f(x,y)],Cos[f(x,y)]}}

    Dune::FieldMatrix<double, DIM, DIM> ddx_D(0.0);
    double x = global[0];
    double y = global[1];

    //create a turning matrix for angle alpha.
    //turning will preserve the SPD properties of the matrix!
    //derivative of turn*A*turn^t
    double alph = alpha(global);
    double dx_alph = dx_alpha(global);
    const double gfc = gf(x, y);
    const double hfc = hf(x, y);
    const double dx_gfc = dx_gf(x, y);
    const double dx_hfc = dx_hf(x, y);

    ddx_D[0][0] = dx_gfc * (cos(alph) * cos(alph)) - 2.0 * gfc * sin(alph) * dx_alph * cos(alph) + 2.0 * hfc * sin(alph) * dx_alph * cos(alph)
                    + (sin(alph) * sin(alph)) * dx_hfc;
    ddx_D[0][1] = gfc * dx_alph * (cos(alph) * cos(alph) - sin(alph) * sin(alph)) - hfc * dx_alph * (cos(alph) * cos(alph) - sin(alph) * sin(alph))
                    + sin(alph) * cos(alph) * (dx_gfc - dx_hfc);

    ddx_D[1][0] = gfc * dx_alph * (cos(alph) * cos(alph) - sin(alph) * sin(alph)) + hfc * dx_alph * (sin(alph) * sin(alph) - cos(alph) * cos(alph))
                    + sin(alph) * cos(alph) * (dx_gfc - dx_hfc);
    ddx_D[1][1] = dx_hfc * (cos(alph) * cos(alph)) + 2.0 * gfc * sin(alph) * dx_alph * cos(alph) - 2.0 * hfc * sin(alph) * dx_alph * cos(alph)
                    + (sin(alph) * sin(alph)) * dx_gfc;

    return ddx_D;
  }

  template<typename POS>
  inline Dune::FieldMatrix<double, DIM, DIM> dy_sinosoidal_D(const POS& global) const {
    Dune::FieldMatrix<double, DIM, DIM> ddy_D(0.0);
    Dune::FieldMatrix<double, DIM, DIM> turn(0.0);
    double x = global[0];
    double y = global[1];

    //create a turning matrix for angle alpha.
    //turning will preserve the SPD properties of the matrix!
    //derivative of turn*A*turn^t

//			ddy_D[0][0] = 2.0*(b - a) *sin(alph)*cos(alph)*dy_alph;
//			ddy_D[0][1] = (a - b)* (std::pow(cos(alph),2) - std::pow(sin(alph),2))*dy_alph;
//			ddy_D[1][0] = (a - b)* (std::pow(cos(alph),2) - std::pow(sin(alph),2))*dy_alph;
//			ddy_D[1][1] = 2.0*(a - b) *sin(alph)*cos(alph)*dy_alph;//notice the sign switch

    double alph = alpha(global);
    double dy_alph = dy_alpha(global);
    const auto gfc = gf(x, y);
    const auto hfc = hf(x, y);
    const auto dy_gfc = dy_gf(x, y);
    const auto dy_hfc = dy_hf(x, y);

    ddy_D[0][0] = dy_gfc * (cos(alph) * cos(alph)) - 2.0 * gfc * sin(alph) * dy_alph * cos(alph) + 2.0 * hfc * sin(alph) * dy_alph * cos(alph)
                    + (sin(alph) * sin(alph)) * dy_hfc;

    ddy_D[0][1] = gfc * dy_alph * (cos(alph) * cos(alph) - sin(alph) * sin(alph)) - hfc * dy_alph * (cos(alph) * cos(alph) - sin(alph) * sin(alph))
                    + sin(alph) * cos(alph) * (dy_gfc - dy_hfc);

    ddy_D[1][0] = gfc * dy_alph * (cos(alph) * cos(alph) - sin(alph) * sin(alph)) + hfc * dy_alph * (sin(alph) * sin(alph) - cos(alph) * cos(alph))
                    + sin(alph) * cos(alph) * (dy_gfc - dy_hfc);

    ddy_D[1][1] = dy_hfc * (cos(alph) * cos(alph)) + 2.0 * gfc * sin(alph) * dy_alph * cos(alph) - 2.0 * hfc * sin(alph) * dy_alph * cos(alph)
                    + (sin(alph) * sin(alph)) * dy_gfc;

    return ddy_D;
  }

  template<typename POS>
  double analytical_growth_sinosoidal(const double u, const POS& loc) const{
    double result = 0.0;
    auto D = sinosoidal_D(loc);
    auto dx_D = dx_sinosoidal_D(loc);
    auto dy_D = dy_sinosoidal_D(loc);
    result += dx_D[0][0] * base.u_x(loc) + D[0][0] * base.u_xx(loc);
    result += dx_D[0][1] * base.u_y(loc) + D[0][1] * base.u_xy(loc);
    result += dy_D[1][0] * base.u_x(loc) + D[1][0] * base.u_xy(loc);
    result += dy_D[1][1] * base.u_y(loc) + D[1][1] * base.u_yy(loc);
    return -result;
  }

  template<typename POS>
  auto gradient(const POS& loc) const {
    if(DIM==2)
      return Dune::FieldVector<double,DIM>({base.u_x(loc), base.u_y(loc)});
    else
      return Dune::FieldVector<double,DIM>({base.u_x(loc), base.u_y(loc), 0.0});
  }

private:
  Dune::ParameterTree& pTree;
  BaseSolution& base;
  double Lx, Ly, Lz;
  double alpha_periods, amp_periods;
  double alpha_c_x, alpha_c_y;
};

#endif
