#ifndef DUNE_VCFV_TEST_DIFFUSIONLOP_HH
#define DUNE_VCFV_TEST_DIFFUSIONLOP_HH

#include<dune/pdelab/common/quadraturerules.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/finiteelement/localbasiscache.hh>
#include<dune/pdelab/localoperator/variablefactories.hh>

#include<dune/vcfv/subentitygenerator.hh>

template<typename FEM, typename ModelCoreType, typename GridType>
class DiffusionLOP: public Dune::PDELab::FullVolumePattern,
                public Dune::PDELab::NumericalJacobianVolume<DiffusionLOP<FEM, ModelCoreType, GridType>>,
                public Dune::PDELab::LocalOperatorDefaultFlags {
public:
  // pattern assembly flags
  enum { doPatternVolume = true };
  // residual assembly flags
  enum { doAlphaVolume = true };
  enum { doLambdaBoundary = true };

  static const int DIM = GridType::dimension;
  using GridViewType = typename GridType::LeafGridView;
  using FiniteElementType = typename FEM::Traits::FiniteElementType; //access the elements
  using LocalBasisType = typename FiniteElementType::Traits::LocalBasisType; //access the base func of ref elm  B^k
  using RangeFieldType = typename LocalBasisType::Traits::RangeFieldType; // B (double/real/complex)
  using JacobianType = typename LocalBasisType::Traits::JacobianType; // B^(k cross d)
  using CoordinateType = typename GridType::ctype;

  using TensorType = Dune::FieldMatrix<RangeFieldType, DIM, DIM>;

private:
  using Generator = Dune::VCFV::Subentitygenerator<GridViewType>;

public:
  DiffusionLOP(Dune::ParameterTree& parTree, const std::shared_ptr<GridViewType>& gridView,
           const std::shared_ptr<ModelCoreType>& mc_p, std::string matrixMeanMethod, double hoelderAlpha = 1.0) :
    pTree(parTree), gv_p(gridView), modelCore_p(mc_p),
    generator_(), matrixMeanMethod_(matrixMeanMethod), hoelderAlpha_(hoelderAlpha) {}

  // volume integral depending on test and ansatz functions
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const {
    // get entity and its geometry:
    const auto& cell = eg.entity();
    const auto& cellGeo = eg.geometry();

    auto subvolumes = generator_.getSubvolumeList();
    generator_.createSubvolumes(cell,subvolumes);
    auto subinterfaces = generator_.getSubinterfaceList();
    generator_.createSubinterfaces(cell,subvolumes,subinterfaces);

    const int numCorners = cellGeo.corners();
    if(numCorners != subvolumes.size())
       DUNE_THROW(Dune::Exception, "#subvolumes =/= #corners");
    if(numCorners != lfsu.size())
       DUNE_THROW(Dune::Exception, "#subvolumes =/= #lfsu");
    if(numCorners != lfsv.size())
       DUNE_THROW(Dune::Exception, "#subvolumes =/= #lfsv");

    //assemble flux
    for(const auto& si : subinterfaces) {
      const auto& interfaceGeo = si->geometryInCell();
      const auto& interfaceGlobalGeo = si->geometry();

      //use analytical tensors
      TensorType D_eff;
      D_eff = modelCore_p->diff(interfaceGlobalGeo.center());

      //evaluate gradient of u at midpoint
      auto& gradphihat = cache_.evaluateJacobian(interfaceGeo.center(), lfsu.finiteElement().localBasis());
      const auto S = cellGeo.jacobianInverseTransposed(interfaceGeo.center());
      auto gradphi = makeJacobianContainer(lfsu);
      for (std::size_t i=0; i<lfsu.size(); ++i)
        S.mv(gradphihat[i][0], gradphi[i][0]);

      // compute gradient of u
      Dune::FieldVector<CoordinateType,DIM> gradu(0.0);
      for(std::size_t i=0; i<lfsu.size(); ++i)
        gradu.axpy(x(lfsu, i), gradphi[i][0]);

      // compute flux at midpoint
      Dune::FieldVector<CoordinateType,DIM> flux(0.0);
      D_eff.mv(gradu,flux);
      auto totalFlux = flux.dot(si->unitOuterNormal()) * interfaceGlobalGeo.volume();

      //assemble flux
      r.accumulate(lfsu, si->inside()->controllingVertexIdx(), -totalFlux);
      r.accumulate(lfsu, si->outside()->controllingVertexIdx(), totalFlux);
    }

    //assemble reaction term
    for(const auto& sv : subvolumes) {
      auto idx = sv->controllingVertexIdx();
      auto f = modelCore_p->f(sv->geometry().center());
      auto factor = sv->geometry().volume();
      r.accumulate(lfsv, idx, -f * factor);
    }
  }

  template<typename IG, typename LFSV, typename R>
  void lambda_boundary (const IG& ig, const LFSV& lfsv_i, R& r_i) const {
    auto face = ig.intersection();

    auto subfaces = generator_.getSubfaceList();
    generator_.createSubfaces(face,subfaces);

    for(const auto& sf : subfaces) {
      auto sfgeo = sf->geometry();

      // evaluate boundary condition type
      bool isdirichlet = modelCore_p->b(sf,sfgeo.center());

      if(!isdirichlet) {
        // evaluate Neumann boundary condition
        auto j = modelCore_p->j(*sf, sfgeo.center());
        r_i.accumulate(lfsv_i, sf->controllingVertexIdx(), j*sfgeo.volume());
      }
      else {
        // evaluate Dirichlet boundary condition (not yet implemented)
      }
    }
  }

public:
  //Parameters given by config file
  Dune::ParameterTree& pTree;
  //shared ptr to the gridview
  std::shared_ptr<GridViewType> gv_p;
  std::shared_ptr<ModelCoreType> modelCore_p;
private:
  Dune::PDELab::LocalBasisCache<LocalBasisType> cache_;

  const Generator generator_;

  const std::string matrixMeanMethod_;
  const double hoelderAlpha_;
};
#endif // DUNE_VCFV_TEST_DIFFUSIONLOP_HH
