#ifndef DUNE_VCFV_TEST_REFERENCETEST_HH
#define DUNE_VCFV_TEST_REFERENCETEST_HH

#include <iostream>

#include<dune/vcfv/referenceconfiguration.hh>
#include<dune/vcfv/subentitygenerator.hh>
#include<dune/vcfv/utility.hh>

// TODO: this should be updated to also test Subfaces
template<typename Grid>
void runTest(Grid& grid, int verbosity=0) {
  using GV = typename Grid::LeafGridView;

  auto gv = grid.leafGridView();
  auto firstCell = *gv.template begin<0>();

  //functionality to be tested
  auto info = Dune::VCFV::SubelementReferenceInfo<typename GV::ctype,GV::dimension>(firstCell.type());
  auto generator = Dune::VCFV::Subentitygenerator<GV>();

  info.printInfo(0);

  int normalCounter = 0;
  int normalErrorCounter = 0;
  for(const auto& cell : elements(gv)) {
    if(verbosity>0)
      std::cout << "--- Information for cell with center (" << cell.geometry().center() <<  ") ---" << std::endl;
    auto subvolumes = generator.getSubvolumeList();
    generator.createSubvolumes(cell,subvolumes);
    auto subinterfaces = generator.getSubinterfaceList();
    generator.createSubinterfaces(cell,subvolumes,subinterfaces);

    auto cellVol = cell.geometry().volume();

    if(verbosity>0)
      std::cout << "--- Subvolume information ---" << std::endl;
    double svVol = 0.0;
    for(auto el : subvolumes) {
      svVol += Dune::VCFV::volume(el->geometry());
      if(verbosity>1) {
        std::cout << "(global) Vertices:" << std::endl;
        for(std::size_t i=0; i<el->geometry().corners(); ++i)
          std::cout << el->geometry().corner(i) << " , ";
        std::cout << std::endl;
      }
      if(verbosity>0)
        std::cout << "global center=" << el->geometry().center()
                  << ", reference cell local center=" << el->geometryInCell().center()
                  << std::endl;
      assert((el->geometry().center() - cell.geometry().global(el->geometryInCell().center())).two_norm() < 1e-10);
    }
    assert(cellVol-svVol < 1e-10);
    if(verbosity>0)
      std::cout << "--- Subvolume information end ---" << std::endl;

    if(verbosity>0)
      std::cout << "--- Subinterface information ---" << std::endl;
    for(auto i : subinterfaces) {
      if(verbosity>0) {
        std::cout << "-> global center=" << i->geometry().center()
                  << ", inside cell global center=" << i->inside()->geometry().center()
                  << ", outside cell global center=" << i->outside()->geometry().center()
                  << std::endl;
        std::cout << "   reference cell local center=" << i->geometryInCell().center() << std::endl;
      }
      assert((i->geometry().center() - cell.geometry().global(i->geometryInCell().center())).two_norm() < 1e-10);

      if(verbosity>0) {
        std::cout << "   inside local=" << i->geometryInInside().center()
                  << ", outside local=" << i->geometryInOutside().center()
                  << std::endl;
        std::cout << "   global center via interface->inside->global: " << i->inside()->geometry().global(i->geometryInInside().center())
                  << std::endl;
        std::cout << "   global center via interface->outside->global: " << i->outside()->geometry().global(i->geometryInOutside().center())
                  << std::endl;
      }
      assert((i->geometry().center() - i->inside()->geometry().global(i->geometryInInside().center())).two_norm() < 1e-10);
      assert((i->geometry().center() - i->outside()->geometry().global(i->geometryInOutside().center())).two_norm() < 1e-10);

      if(verbosity>0) {
        std::cout << "   global center via interface->inside->cell: " << i->inside()->geometryInCell().global(i->geometryInInside().center())
                  << std::endl;
        std::cout << "   global center via interface->outside->cell: " << i->outside()->geometryInCell().global(i->geometryInOutside().center())
                  << std::endl;
      }
      assert((i->geometryInCell().center() - i->inside()->geometryInCell().global(i->geometryInInside().center())).two_norm() < 1e-10);
      assert((i->geometryInCell().center() - i->outside()->geometryInCell().global(i->geometryInOutside().center())).two_norm() < 1e-10);

      if(verbosity>0)
        std::cout << "   Normal: " << i->unitOuterNormal() << std::endl;

      bool isNormalFalse = false;
      for(size_t c=1; c < i->geometry().corners(); ++c) {
        auto dummy = i->geometry().corner(c) - i->geometry().corner(0);
        dummy /= dummy.two_norm();
        //check normal property
        if(std::abs(i->unitOuterNormal().dot(dummy)) > 1.75e-2) {
          std::cout << "Bad scalar product, deviation of "
                    << 90.0 - std::acos(i->unitOuterNormal().dot(dummy)) * 180.0/3.141 << "°" << std::endl;
          std::cout << "---->Normal: " << i->unitOuterNormal() << ", dummy: " << dummy << std::endl;
          isNormalFalse = true;
        }
        //assert(std::abs(i->unitOuterNormal().dot(dummy)) < 1.75e-2); //corresponds to deviation of ~1°
        //check unit length
        assert(std::abs(i->unitOuterNormal().two_norm()-1) < 1e-10);
      }
      if(isNormalFalse)
        ++normalErrorCounter;
      ++normalCounter;

      auto direction = i->outside()->geometry().center() - i->inside()->geometry().center();
      direction /= direction.two_norm();
      //check orientation
      assert(i->unitOuterNormal().dot(direction) > 1e-10);
    }
    if(verbosity>0)
      std::cout << "--- Subinterface information end ---" << std::endl;
  }
  std::cout << normalErrorCounter << " of " << normalCounter << " normals were faulty (" << 100.0 * (double)normalErrorCounter/(double)normalCounter
            << "%)" << std::endl;
}
#endif
