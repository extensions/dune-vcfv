#ifndef DUNE_VCFV_TEST_DIFFUSIONTEST_HH
#define DUNE_VCFV_TEST_DIFFUSIONTEST_HH

#include <iostream>

#include<dune/common/parametertreeparser.hh>

#include<dune/grid/io/file/vtk.hh>
#include<dune/grid/io/file/vtk/vtkwriter.hh>

#include<dune/pdelab/backend/istl.hh>
#include<dune/pdelab/common/vtkexport.hh>
#include<dune/pdelab/constraints/common/constraints.hh>
#include<dune/pdelab/constraints/common/constraintsparameters.hh>
#include<dune/pdelab/constraints/conforming.hh>
#include<dune/pdelab/function/callableadapter.hh>
#include<dune/pdelab/function/const.hh>
#include<dune/pdelab/function/minus.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionadapter.hh>
#include<dune/pdelab/gridfunctionspace/vtk.hh>
#include<dune/pdelab/stationary/linearproblem.hh>
#include<dune/pdelab/test/l2norm.hh>

#include<dune/vcfv/pdelab/interpolation.hh>
#include<dune/vcfv/pdelab/l2norm.hh>
#include<dune/vcfv/pdelab/mean.hh>

#include "diffusionLOP.hh"	//The local operator
#include "diffusionproblem.hh" //the model passed to the LOP

template<typename FEM, typename Grid>
void doTest(std::shared_ptr<Grid> gridp, Dune::ParameterTree pTree,
            Dune::ParameterTree studypTree, std::string output = "(grid_type_not_given)") {
    //---GLOBAL DEFINITIONS:---
    const int DIM = Grid::dimension;
    const int degree = 1; //1=linear
    bool bWriteOutput = studypTree.get<bool>("bWriteOutput");

    using Problem = Diffusionproblem<DIM>;

    using GridViewType = typename Grid::LeafGridView;
    typedef Dune::PDELab::ConformingDirichletConstraints CON;
    typedef Dune::PDELab::ISTL::VectorBackend<> VBE;
    typedef Dune::PDELab::GridFunctionSpace<GridViewType, FEM, CON, VBE> GFS;
    typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
    typedef typename GFS::template ConstraintsContainer<double>::Type ConstraintsContainer;
    typedef Dune::PDELab::Backend::Vector<GFS,double> Z;
    typedef Dune::PDELab::DiscreteGridFunction<GFS, Z> ZDGF;

    typedef Dune::PDELab::GridOperator<GFS, GFS, /* ansatz and test space */
                DiffusionLOP<FEM, Problem, Grid>, /* local operator */
                MBE, /* matrix backend */
                double, double, double, /* domain, range, jacobian field type*/
                ConstraintsContainer, ConstraintsContainer /* constraints for ansatz and test space */> GO;

    std::shared_ptr<Problem> problem_p;
    //create a modelCore, which holds the pde definition:
    problem_p = std::make_shared<Problem>(pTree);
    auto glambda = [&](const auto& e, const auto& x) {return problem_p->g(e,x);};
    auto blambda = [&](const auto& i, const auto& x) {return problem_p->b(i,x);};
    auto solutionLambda = [&](const auto& x) {return problem_p->u_ref(x);};
    auto reactionLambda = [&](const auto& x) {return problem_p->f(x);};

    ConstraintsContainer cc;

    std::shared_ptr<GridViewType> gv_p;
    std::shared_ptr<FEM> fem_p;
    std::shared_ptr<DiffusionLOP<FEM, Problem, Grid>> bfvm_LOP_p;
    std::shared_ptr<GFS> gfs_p;
    std::shared_ptr<GO> go_p;

    // Select a linear solver backend
    typedef Dune::PDELab::ISTLBackend_SEQ_CG_AMG_SSOR<GO> LS;
    LS ls(pTree.get<int>("solver.maxIter",100), DIM);

    std::size_t ref_min, ref_max;
    std::string matrixMeanMethod = "standardEuclidian";
    double hoelderAlpha = 1.0;
    ref_min = studypTree.get<int>("refMin");
    ref_max = studypTree.get<int>("refMax");
    matrixMeanMethod = studypTree.get<std::string>("tensorInterpolation");
    hoelderAlpha = studypTree.get<double>("hoelderAlpha");
    gridp->globalRefine(ref_min);

    for(std::size_t ref=ref_min; ref<=ref_max; ++ref) {
      gv_p = std::make_shared<GridViewType>(gridp->leafGridView());
      fem_p = std::make_shared<FEM>(*gv_p);

      double gridwidth = 0.0;
      for(const auto& e : edges(*gv_p)){
        if(e.geometry().volume() > gridwidth)
          gridwidth = e.geometry().volume();
      }
      std::cout << "grid was refined " << ref << " times, gridwidth is " << gridwidth << std::endl;

      //create the Local Operator, which then retrieves the
      //necessary info from the Diffusionproblem:
      bfvm_LOP_p  = std::make_shared<DiffusionLOP<FEM, Problem, Grid>>(pTree, gv_p, problem_p, matrixMeanMethod, hoelderAlpha);

      //create pointer to gridFunctionSpace
      gfs_p = std::make_shared<GFS>(*gv_p, *fem_p);

      auto g = Dune::PDELab::makeGridFunctionFromCallable(*gv_p, glambda);
      auto b = Dune::PDELab::makeBoundaryConditionFromCallable(*gv_p, blambda);
      auto solution = Dune::PDELab::makeGridFunctionFromCallable(*gv_p, solutionLambda);
      auto reaction = Dune::PDELab::makeGridFunctionFromCallable(*gv_p, reactionLambda);

      //  coefficient vectors
      std::shared_ptr<Z> z_p = std::make_shared<Z>(*gfs_p);               // numeric solution value
      std::shared_ptr<Z> z_init_p = std::make_shared<Z>(*gfs_p);    // initial value
      std::shared_ptr<Z> z_sol_p = std::make_shared<Z>(*gfs_p);     // analytical solution
      std::shared_ptr<Z> z_react_p = std::make_shared<Z>(*gfs_p);     // analytical solution

      // Make a grid functions (VTK output)
      ZDGF zdgf(*gfs_p, *z_p);
      ZDGF zdgf_init(*gfs_p, *z_init_p);
      ZDGF zdgf_sol(*gfs_p, *z_sol_p);
      ZDGF zdgf_react(*gfs_p, *z_react_p);

      using Generator = typename Dune::VCFV::Subentitygenerator<GridViewType>;
      Generator generator = Generator();

      bool enforceMeanZero = problem_p->enforceMeanZero();

      // Assemble constraints
      Dune::PDELab::constraints(b, *gfs_p, cc);

      // Fill the coefficient vectors
      std::size_t quadratureOrder = 2;
      if(!enforceMeanZero) {
        Dune::VCFV::interpolate(g, *gfs_p, *z_p, quadratureOrder, generator);			//interpolates also into constrained dofs!
        Dune::VCFV::interpolate(g, *gfs_p, *z_init_p, quadratureOrder, generator);
        Dune::PDELab::set_nonconstrained_dofs(cc, 0.1, *z_p);
        Dune::PDELab::set_nonconstrained_dofs(cc, 0.1, *z_init_p);		//purely for output
      }
      else {
        Dune::PDELab::set_nonconstrained_dofs(cc, 0.0, *z_p);
        Dune::PDELab::set_nonconstrained_dofs(cc, 0.0, *z_init_p);		//purely for output
      }
      Dune::VCFV::interpolate(solution, *gfs_p, *z_sol_p, quadratureOrder, generator);
      Dune::VCFV::interpolate(reaction, *gfs_p, *z_react_p, quadratureOrder, generator);

      //how many entries per row
      MBE mbe((int) pow(1 + 2 * degree, DIM));

      go_p = std::make_shared<GO>(*gfs_p, cc, *gfs_p, cc, *bfvm_LOP_p, mbe);

      using Solver = Dune::PDELab::StationaryLinearProblemSolver<GO,LS,Z>;
      Solver solver(*go_p,ls,*z_p,1e-8);
      solver.apply();

      if(bWriteOutput) {
        // Write VTK output file
        int subsampling = 1;
        Dune::SubsamplingVTKWriter<GridViewType> vtkwriter(*gv_p, Dune::RefinementIntervals(subsampling), false);
        using VTKF = Dune::PDELab::VTKGridFunctionAdapter<ZDGF>;

        using DifferenceDGF = Dune::PDELab::MinusGridFunctionAdapter<ZDGF,ZDGF>;
        using DifferenceVTKF = Dune::PDELab::VTKGridFunctionAdapter<DifferenceDGF>;
        using ConstantDGF = Dune::PDELab::ConstGridFunction<GridViewType,double>;
        using ShiftedDGF = Dune::PDELab::MinusGridFunctionAdapter<ZDGF,ConstantDGF>;
        using ShiftedVTKF = Dune::PDELab::VTKGridFunctionAdapter<ShiftedDGF>;
        using ShiftedErrorDGF = Dune::PDELab::MinusGridFunctionAdapter<ShiftedDGF,ShiftedDGF>;
        using ShiftedErrorVTKF = Dune::PDELab::VTKGridFunctionAdapter<ShiftedErrorDGF>;

        vtkwriter.addVertexData(std::shared_ptr<VTKF>(new VTKF(zdgf_init, "initial_value")));
        vtkwriter.addVertexData(std::shared_ptr<VTKF>(new VTKF(zdgf_react, "reaction_term")));

        double globall2err(0.0), manuall2error(0.0);
        if(enforceMeanZero) {
          ConstantDGF zdgf_mean(*gv_p,Dune::VCFV::mean(zdgf));
          ShiftedDGF zdgf_shifted(zdgf,zdgf_mean);
          ConstantDGF zdgf_sol_mean(*gv_p,Dune::VCFV::mean(zdgf_sol));
          ShiftedDGF zdgf_sol_shifted(zdgf_sol,zdgf_sol_mean);
          ShiftedErrorDGF error(zdgf_sol_shifted, zdgf_shifted);

          vtkwriter.addVertexData(std::shared_ptr<ShiftedVTKF>(new ShiftedVTKF(zdgf_shifted, "num_sol")));
          vtkwriter.addVertexData(std::shared_ptr<ShiftedVTKF>(new ShiftedVTKF(zdgf_sol_shifted, "analytical_sol")));
          vtkwriter.addVertexData(std::shared_ptr<ShiftedErrorVTKF>(new ShiftedErrorVTKF(error, "error")));
          globall2err = l2norm(error);
          manuall2error = Dune::VCFV::l2difference(zdgf_sol_shifted,zdgf_shifted,generator);
        }
        else {
          DifferenceDGF error(zdgf_sol,zdgf);

          vtkwriter.addVertexData(std::shared_ptr<VTKF>(new VTKF(zdgf, "num_sol")));
          vtkwriter.addVertexData(std::shared_ptr<VTKF>(new VTKF(zdgf_sol, "analytical_sol")));
          vtkwriter.addVertexData(std::shared_ptr<DifferenceVTKF>(new DifferenceVTKF(error, "error")));
          globall2err =  l2norm(error);
          manuall2error = Dune::VCFV::l2difference(zdgf_sol,zdgf,generator);
        }

        //write solutions.
        std::string configType = studypTree.get<std::string>("config");
        std::cout <<"writing output to file"<< std::endl;
        vtkwriter.write(configType + "_" + matrixMeanMethod + "_" + output + "_dim_" + std::to_string(DIM) + "_ref_" + std::to_string(ref), Dune::VTK::appendedraw);
        std::cout <<"output file written!"<< std::endl;

        //analytics
        std::cout << "L2-error: " << globall2err << std::endl;
        std::cout << "manual L2-error: " << manuall2error << std::endl;
      }

      if(ref!=ref_max) {
        gridp->globalRefine(1);
        std::cout << std::endl;
      }
    }
}
#endif
