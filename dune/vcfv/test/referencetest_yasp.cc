// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include<dune/common/parallel/mpihelper.hh>
#include<dune/grid/yaspgrid.hh>

#include "referencetest.hh"

int main(int argc, char** argv) {
  Dune::MPIHelper::instance(argc, argv);

  const std::size_t verbosity = 0;

  const int dim = CMAKE_DIMENSION;
  using Grid = Dune::YaspGrid<dim>;

  //generate grid
  Dune::FieldVector<double, dim> domain;
  std::array<int, dim> domainDims;
  for(std::size_t i=0; i<dim; ++i) {
    domain[i] = 1.0;
    domainDims[i] = 10;
  }
  Grid grid(domain, domainDims);

  //run test
  runTest(grid,verbosity);
}
