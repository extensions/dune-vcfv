// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <iostream>

#if HAVE_DUNE_ALUGRID
#include<dune/alugrid/grid.hh>
#include<dune/alugrid/dgf.hh>
#include<dune/grid/io/file/dgfparser/dgfparser.hh>
#endif

#include<dune/grid/io/file/gmshreader.hh>

#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions
#include<dune/common/parametertreeparser.hh>

#include<dune/pdelab/backend/istl.hh>
#include<dune/pdelab/finiteelementmap/qkfem.hh>

#include "diffusiontest.hh"

int main(int argc, char** argv) {
  try {
    Dune::MPIHelper::instance(argc, argv);

    const int dim = CMAKE_DIMENSION;
    const int degree = 1; //1=linear
    std::string output = "quad";
    std::string filename = "meshes/" + std::to_string(dim) + "d/quad.msh";

    //read parameter trees
    Dune::ParameterTreeParser ptreeparser;
    Dune::ParameterTree pTree, studypTree;
    std::string studyconfigpath = "configs/config_study.ini";
    ptreeparser.readINITree(studyconfigpath, studypTree);
    std::string configType = studypTree.get<std::string>("config");
    std::string configpath = "configs/" + configType + ".ini";
    ptreeparser.readINITree(configpath, pTree);

    //simplex-alugrid-specific types
    using Grid = Dune::ALUGrid<dim, dim, Dune::cube, Dune::nonconforming>;
    using GridViewType = typename Grid::LeafGridView;
    using FEM = Dune::PDELab::QkLocalFiniteElementMap<GridViewType, double, double, degree>;

    //generate grid
    Dune::GridFactory<Grid> factory;
    Dune::GmshReader<Grid>::read(factory, filename, true, true);
    std::shared_ptr<Grid> gridp(factory.createGrid());

    //do the test
    doTest<FEM>(gridp, pTree, studypTree, output);

    return EXIT_SUCCESS;
  }
  catch(Dune::Exception &e) {
    std::cerr << "Dune reported error: " << e << std::endl;
    return EXIT_FAILURE;
  }
}
