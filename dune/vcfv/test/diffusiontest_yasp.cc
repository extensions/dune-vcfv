// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <iostream>

#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions
#include<dune/common/parametertreeparser.hh>

#include<dune/grid/yaspgrid.hh>                // YetAnotherStructuredGrid

#include<dune/pdelab/backend/istl.hh>
#include<dune/pdelab/finiteelementmap/qkfem.hh>

#include "diffusiontest.hh"

int main(int argc, char** argv) {
  try {
    Dune::MPIHelper::instance(argc, argv);

    const int dim = CMAKE_DIMENSION;
    const int degree = 1; //1=linear
    std::string output = "yasp";

    //read parameter trees
    Dune::ParameterTreeParser ptreeparser;
    Dune::ParameterTree pTree, studypTree;
    std::string studyconfigpath = "configs/config_study.ini";
    ptreeparser.readINITree(studyconfigpath, studypTree);
    std::string configType = studypTree.get<std::string>("config");
    std::string configpath = "configs/" + configType + ".ini";
    ptreeparser.readINITree(configpath, pTree);

    //yasp-specific types
    using Grid = Dune::YaspGrid<dim>;
    using GridViewType = typename Grid::LeafGridView;
    using FEM = Dune::PDELab::QkLocalFiniteElementMap<GridViewType, double, double, degree>;

    //generate grid
    Dune::FieldVector<double, dim> domain;
    std::array<int, dim> domainDims;
    domain[0] = 1.0;
    domain[1] = 1.0;
    domainDims[0] = pTree.get<int>("grid.yasp_x");
    domainDims[1] = pTree.get<int>("grid.yasp_y");
    if(dim==3) {
      domain[2] = 1.0;
      domainDims[2] = pTree.get<int>("grid.yasp_z");
    }
    int overlap = 1;               //always set to one
    std::bitset<dim> B(false);
    std::shared_ptr<Grid> gridp = std::shared_ptr<Grid>(new Grid(domain, domainDims, B,
                                                                   overlap, Dune::MPIHelper::getCollectiveCommunication()));

    //do the test
    doTest<FEM>(gridp, pTree, studypTree, output);

    return EXIT_SUCCESS;
  }
  catch(Dune::Exception &e) {
    std::cerr << "Dune reported error: " << e << std::endl;
    return EXIT_FAILURE;
  }
}
