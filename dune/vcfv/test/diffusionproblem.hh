#ifndef DUNE_VCFV_TEST_DIFFUSIONPROBLEM_HH
#define DUNE_VCFV_TEST_DIFFUSIONPROBLEM_HH

#include<dune/vcfv/test/sinosoidal_diffusion.hh>

template<int DIM>
class Diffusionproblem
{
public:
  using value_type = double;
  using GradientType = Dune::FieldVector<value_type,DIM>;
  using TensorType = Dune::FieldMatrix<value_type,DIM,DIM>;
  using BaseSolution = typename Test::HomogeneousDirichletBase<DIM>;
  //using BaseSolution = typename Test::HomogeneousNeumannBase<DIM>;

  Diffusionproblem(Dune::ParameterTree& pTree) : pTree_(pTree) , baseSolution_(pTree), kernel_(pTree, baseSolution_) {}

  //position is global
  TensorType diff(const Dune::FieldVector<double,DIM> position) const {
    return kernel_.sinosoidal_D(position);
  }

  //! right hand side
  template<typename X>
  double f(const X& x) const
  {
    return kernel_.analytical_growth_sinosoidal(0.0, x); //not depending u, different interface
  }

  //! boundary condition type function (true = Dirichlet)
  template<typename I, typename X>
  bool b(const I& i, const X& x) const
  {
    //return true; //only Dirichlet boundary
    return false; //only Neumann boundary
    //auto x_global = i.geometry().global(x);
    //return (x_global[0] < 1e-10 || x_global[0] > 1-1e-10 || x_global[1] < 1e-10 || x_global[1] > 1-1e-10); //mixed boundary (3d)
  }

  //! Dirichlet boundary VALUE
  template<typename E, typename X>
  double g(const E& e, const X& x) const
  {
    return baseSolution_.u(e.geometry().global(x));
  }

  //! Neumann boundary condition
  template<typename I, typename X>
  double j(const I& i, const X& x) const
  {
    GradientType grad(0.0);
    diff(x).mv(kernel_.gradient(x), grad);
    return -i.unitOuterNormal().dot(grad);
  }

  //! reference solution
  template<typename X>
  double u_ref(const X& x) const
  {
    return baseSolution_.u(x);
  }

  //! should the solution have mean zero
  bool enforceMeanZero() const {
    return true; //enable for Neumann-only problems
    //return false;
  }

private:
  Dune::ParameterTree& pTree_;
  BaseSolution baseSolution_;
  KernelProvider<BaseSolution,DIM> kernel_;
};
#endif
